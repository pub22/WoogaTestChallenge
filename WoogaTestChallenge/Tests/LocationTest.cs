using NUnit.Framework;
using WoogaTestChallenge.Pages;

namespace WoogaTestChallenge.Tests
{
    public class LocationTest : BaseTest
    {
        [Test]
        public void SearchPlaceTest()
        {
            var searchInput = "Wooga";
            var homepage = new Homepage(Driver);

            var searchResults = homepage
                .Open()
                .ChangeLanguageToEnglish()
                .ClickSearchField()
                .SearchPlace(searchInput);
            searchResults.TakeScreenshot();

            Assert.Multiple(()
                =>
            {
                Assert.That(homepage.PageTitle, Contains.Substring(searchInput));
                Assert.That(searchResults.Adress, Is.EqualTo("Address: Saarbrücker Str. 38, 10405 Berlin, Germany "));
                Assert.That(searchResults.LocatedIn, Is.EqualTo("Located in: BACKFABRIK"));
                Assert.That(searchResults.Authority, Is.EqualTo("Website: wooga.com "));
            });
        }

        [Test]
        public void LastSearchTest()
        {
            var searchInput = "Berliner Dom";
            var homepage = new Homepage(Driver);

            var searchResults = homepage
                .Open()
                .ChangeLanguageToEnglish()
                .ClickSearchField()
                .SearchPlace(searchInput);

            homepage.ClickSearchField();
            homepage.TakeScreenshot();
            Assert.That(searchResults.LastSearch, Is.EqualTo(searchInput));
        }
    }
}