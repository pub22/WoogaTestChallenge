﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;
using WoogaTestChallenge.Config;

namespace WoogaTestChallenge.WebDriver
{
    public class WebDriverFactory
    {
        public IWebDriver GetWebDriver()
        {
            var driverConfig = ConfigReader.DriverConfig;

            switch (driverConfig.BrowserType.ToLower())
            {
                case "local":
                    return GetLocalWebDriver(driverConfig);
                case "remote":
                    return GetRemoteWebDriver(driverConfig);
                default:
                    throw new ArgumentOutOfRangeException(nameof(driverConfig.BrowserType), "invalid browser type");
            }
        }

        private IWebDriver GetLocalWebDriver(DriverConfig driverConfig)
        {
            switch (driverConfig.Browser.ToLower())
            {
                case "chrome":
                    new DriverManager().SetUpDriver(new ChromeConfig());
                    return new ChromeDriver(BrowserOptions.ChromeOptions(driverConfig));
                case "firefox":
                    new DriverManager().SetUpDriver(new FirefoxConfig());
                    return new FirefoxDriver(BrowserOptions.FirefoxOptions(driverConfig));
                case "edge":
                    new DriverManager().SetUpDriver(new EdgeConfig());
                    return new EdgeDriver(BrowserOptions.EdgeOptions(driverConfig));
                default:
                    throw new ArgumentOutOfRangeException(nameof(driverConfig.Browser), "invalid browser name");
            }
        }

        private IWebDriver GetRemoteWebDriver(DriverConfig driverConfig)
        {
            switch (driverConfig.Browser.ToLower())
            {
                case "chrome":
                    return new RemoteWebDriver(new Uri(UrlProvider.GridUrl),
                        BrowserOptions.ChromeOptions(driverConfig));
                case "firefox":
                    return new RemoteWebDriver(new Uri(UrlProvider.GridUrl),
                        BrowserOptions.FirefoxOptions(driverConfig));
                case "edge":
                    return new RemoteWebDriver(new Uri(UrlProvider.GridUrl),
                        BrowserOptions.EdgeOptions(driverConfig));
                default:
                    throw new ArgumentOutOfRangeException(nameof(driverConfig.Browser), "invalid browser name");
            }
        }
    }
}
