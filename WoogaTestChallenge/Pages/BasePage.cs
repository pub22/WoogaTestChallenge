﻿using System;
using System.IO;
using System.Reflection;
using OpenQA.Selenium;
using WoogaTestChallenge.Extensions;

namespace WoogaTestChallenge.Pages
{
    public abstract class BasePage
    {
        protected readonly IWebDriver Driver;

        protected BasePage(IWebDriver driver)
        {
            Driver = driver;
        }

        protected string GetPageTitle()
        {
            return Driver.Title;
        }

        public void TakeScreenshot() => Driver.TakeScreenshot();

    }
}