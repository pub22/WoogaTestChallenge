﻿using System;
using OpenQA.Selenium;
using WoogaTestChallenge.Extensions;

namespace WoogaTestChallenge.Pages
{
    public class SearchResults : BasePage
    {
        private IWebElement AddressElement => Driver.WaitAndFind(By.CssSelector("button[data-item-id='address']"));
        private IWebElement LocatedInElement => Driver.WaitAndFind(By.CssSelector("button[data-item-id='locatedin']"));
        private IWebElement AuthorityElement => Driver.WaitAndFind(By.CssSelector("button[data-item-id='authority']"));
        private IWebElement SearchButton => Driver.WaitAndFind(By.CssSelector("div#directions-searchbox-0 > button[aria-label='Search']"));
        private IWebElement LastSearchElement => Driver.WaitAndFind(By.XPath("(//div[@role='gridcell']/span/span)[1]"));
        private IWebElement StartingPointSearchElement => Driver.WaitAndFind(By.CssSelector("div#directions-searchbox-0 input.tactile-searchbox-input"));
        private IWebElement DestinationSearchElement => Driver.WaitAndFind(By.CssSelector("div#directions-searchbox-1 input.tactile-searchbox-input"));
        private IWebElement DirectionsButtonElement => Driver.WaitAndFind(By.CssSelector("button[data-value='Directions']"));
        private IWebElement FirstDirectionsResultElement => Driver.WaitAndFind(By.Id("section-directions-trip-0"));
        private IWebElement FirstDirectionsResultDistanceElement => Driver.WaitAndFind(By.CssSelector("#section-directions-trip-0 > div > div:nth-of-type(3) > div > div:nth-of-type(2)"));
        private IWebElement TravelModeWalkingElement => Driver.WaitAndFind(By.CssSelector("img[aria-label='Walking']"));
        private IWebElement TravelModeDrivingElement => Driver.WaitAndFind(By.CssSelector("img[aria-label='Driving']"));
        private IWebElement TravelModeTransitElement => Driver.WaitAndFind(By.CssSelector("img[aria-label='Transit']"));
        private IWebElement RouteOptionsElement => Driver.WaitAndFind(By.XPath("//button/span[contains(text(),'Options')]"));
        private IWebElement UnitKilometersElement => Driver.WaitAndFind(By.CssSelector("label[for='pane.directions-options-units-km']"));
        private IWebElement AssistiveChipsElement => Driver.WaitAndFind(By.CssSelector("div#assistive-chips > div"));
        private IWebElement ReverseDestinationElement => Driver.WaitAndFind(By.CssSelector("button[aria-label='Reverse starting point and destination']"));
        public string Adress => AddressElement.GetAttribute("aria-label");
        public string LocatedIn => LocatedInElement.GetAttribute("aria-label");
        public string Authority => AuthorityElement.GetAttribute("aria-label");
        public string LastSearch => LastSearchElement.Text;
        public string FirstResultDistance => FirstDirectionsResultDistanceElement.Text;

        public string StartingPointValue =>
            StartingPointSearchElement.GetAttribute("aria-label").Replace("Starting point ", "");

        public string DestinationValue =>
            DestinationSearchElement.GetAttribute("aria-label").Replace("Destination ", "");

        public SearchResults(IWebDriver driver) : base(driver)
        {
        }

        public SearchResults ClickDirectionsButton()
        {
            Driver.WaitAndClick(DirectionsButtonElement);
            Driver.WaitUntilClickable(StartingPointSearchElement);
            return this;
        }

        public SearchResults FillStartingPoint(string startingPoint)
        {
            Driver.WaitAndSendKeys(StartingPointSearchElement, startingPoint);
            SearchButton.Submit();
            Driver.WaitUntilClickable(FirstDirectionsResultElement);
            return this;
        }

        public SearchResults ChangeTravelMode(string travelMode)
        {
            switch (travelMode.ToLower())
            {
                case "walking":
                    Driver.WaitAndClick(TravelModeWalkingElement);
                    break;
                case "driving":
                    Driver.WaitAndClick(TravelModeDrivingElement);
                    break;
                case "transit":
                    Driver.WaitAndClick(TravelModeTransitElement);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(travelMode), "invalid value for travel mode provided");
            }

            return this;
        }

        public SearchResults OpenRouteOptions()
        {
            Driver.WaitUntilClickable(AssistiveChipsElement);
            Driver.WaitAndClick(RouteOptionsElement);
            Driver.WaitUntilClickable(UnitKilometersElement);
            return this;
        }

        public SearchResults ChangeUnitsToKilometers()
        {
            Driver.WaitAndClick(UnitKilometersElement);
            Driver.WaitUntilClickable(AssistiveChipsElement);
            return this;
        }

        public SearchResults ClickReverseDestination()
        {
            Driver.WaitUntilClickable(AssistiveChipsElement);
            Driver.WaitAndClick(ReverseDestinationElement);
            Driver.WaitUntilClickable(FirstDirectionsResultElement);

            return this;
        }
    }
}