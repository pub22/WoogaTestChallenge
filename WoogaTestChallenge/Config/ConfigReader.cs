﻿using Microsoft.Extensions.Configuration;

namespace WoogaTestChallenge.Config
{
    public class ConfigReader
    {
        public static DriverConfig DriverConfig => ReadConfigFile<DriverConfig>("appsettings.json");

        private static T ReadConfigFile<T>(string file)
        {
            return new ConfigurationBuilder().AddJsonFile(file).Build().Get<T>();
        }
    }
}