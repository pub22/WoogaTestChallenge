﻿namespace WoogaTestChallenge.Config
{
    public class UrlProvider
    {
        private static string BaseUrl => ConfigReader.DriverConfig.BaseUrl;

        public static string Homepage => BaseUrl;

        public static string GridUrl =>
            $"http://selenium__standalone-{ConfigReader.DriverConfig.Browser.ToLower()}:4444/wd/hub";
    }
}
