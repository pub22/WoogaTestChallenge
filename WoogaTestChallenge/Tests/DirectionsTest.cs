﻿using NUnit.Framework;
using WoogaTestChallenge.Pages;

namespace WoogaTestChallenge.Tests
{
    public class DirectionsTest : BaseTest
    {
        [Test]
        public void WalkingTripShortestDistanceTest()
        {
            var destinationAddress = "Wittstocker Str. 7A Berlin";
            var startAddress = "Waldstraße 25, 10551 Berlin";
            
            var homepage = new Homepage(Driver);
            var searchResults = homepage
                .Open()
                .ChangeLanguageToEnglish()
                .ClickSearchField()
                .SearchPlace(destinationAddress);

            searchResults
                .ClickDirectionsButton()
                .FillStartingPoint(startAddress)
                .ChangeTravelMode("walking")
                .OpenRouteOptions()
                .ChangeUnitsToKilometers();

            searchResults.TakeScreenshot();

            Assert.That(searchResults.FirstResultDistance, Is.EqualTo("500 m"));
        }

        [Test]
        public void reverseDestinationTest()
        {
            var destinationAddress = "Obrońców Wybrzeża 17, 80-398 Gdańsk";
            var startAddress = "Tadeusza Kościuszki 6, 87-100 Toruń";

            var homepage = new Homepage(Driver);
            var searchResults = homepage
                .Open()
                .ChangeLanguageToEnglish()
                .ClickSearchField()
                .SearchPlace(destinationAddress);

            searchResults
                .ClickDirectionsButton()
                .FillStartingPoint(startAddress)
                .ClickReverseDestination();

            searchResults.TakeScreenshot();

            Assert.Multiple(() =>
            {
                Assert.That(searchResults.StartingPointValue, Is.EqualTo(destinationAddress));
                Assert.That(searchResults.DestinationValue, Is.EqualTo(startAddress));
            });
        }
    }
}
