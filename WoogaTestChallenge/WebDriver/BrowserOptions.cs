﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using WoogaTestChallenge.Config;

namespace WoogaTestChallenge.WebDriver
{
    public class BrowserOptions
    {
        public static ChromeOptions ChromeOptions(DriverConfig config)
        {
            var options = new ChromeOptions();
            options.AddArgument($"--lang={config.Language}");
            options.AddUserProfilePreference("intl.accept_languages", config.Language);
            options.PageLoadStrategy = PageLoadStrategy.Normal;
            options.AddExcludedArgument("enable-automation");
            options.AddAdditionalChromeOption("useAutomationExtension", false);

            if (config.Headless)
            {
                options.AddArgument("-headless");
            }

            return options;
        }

        public static FirefoxOptions FirefoxOptions(DriverConfig config)
        {
            var options = new FirefoxOptions { AcceptInsecureCertificates = true };
            options.SetPreference("intl.accept_languages", config.Language);

            if (config.Headless)
            {
                options.AddArgument("-headless");
            }

            return options;
        }

        public static EdgeOptions EdgeOptions(DriverConfig config)
        {
            return new EdgeOptions
            {
                PageLoadStrategy = PageLoadStrategy.Normal
            };
        }
    }
}
