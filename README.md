# WoogaTestChallenge

## Description

This solution contains project with UI tests created as part of test challenge for Wooga. Tests are created in C# using Selenium WebDriver and NUnit framework. The project is written using Page Object Model pattern. It supports using both standard WebDriver and RemoteWebDriver (for Selenium Grid integration) and running browsers in headless mode. 

Supported browsers:
 - Firefox
 - Chrome
 - Edge

## Usage

Tests can be run locally from Visual Studio. Configuration of test run can be found in **appsettings.json** file and following items are available there:

- **browser:** defines which browser will be used, available values: chrome, firefox, edge
- **language:** defines language for browser options
- **baseUrl:** url of the main page for tests
- **defaultTimeout:** defines default timeout value in seconds used by WebDriverWait class
- **browserType:** defines type of the browser used in tests, as a result WebDriver or RemoteWebDriver object is being instantiated, available values: local, remote
- **headless:** defines whether browser should be run in headless mode or not, available values: true, false


## Future Improvements

 - fix ci pipeline (right now it's building correctly and selenium tests are being run but there are WebDriver exceptions that needs verification)
 - add more tests (corner/edge cases, negative paths)
 - add logger
 - add reporting
