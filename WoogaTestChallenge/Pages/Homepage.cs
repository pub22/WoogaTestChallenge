﻿using OpenQA.Selenium;
using WoogaTestChallenge.Config;
using WoogaTestChallenge.Extensions;

namespace WoogaTestChallenge.Pages
{
    public class Homepage : BasePage
    {
        private IWebElement SearchField => Driver.WaitAndFind(By.Id("searchboxinput"));
        private IWebElement SearchButton => Driver.WaitAndFind(By.Id("searchbox-searchbutton"));
        private IWebElement ClearSearchButton => Driver.WaitAndFind(By.CssSelector("a[aria-label='Clear search']"));
        private IWebElement Minimap => Driver.WaitAndFind(By.Id("minimap"));
        private IWebElement MenuButton => Driver.WaitAndFind(By.CssSelector("button[aria-label='Menu']"));
        private IWebElement LanguageButton => Driver.WaitAndFind(By.CssSelector("button[jsaction='settings.languages']"));
        private IWebElement CloseLanguagePanelButton => Driver.WaitAndFind(By.XPath("(//div[@id='languages'])[1]/parent::div/parent::div/preceding-sibling::div[1]/button"));
        private IWebElement EnglishLanguageItem => Driver.WaitAndFind(By.XPath("//*[contains(text(),'English (United States)')]"));
        private IWebElement SeeTraficButton => Driver.WaitAndFind(By.CssSelector("div[role='region'] > button"));
        public string PageTitle => GetPageTitle();

        public Homepage(IWebDriver driver) : base(driver)
        {
        }

        public Homepage Open()
        {
            Driver.GoTo(UrlProvider.Homepage);
            return this;
        }

        public SearchResults SearchPlace(string searchInput)
        {
            SearchField.SendKeys(searchInput);
            SearchButton.Submit();
            Driver.WaitUntilClickable(ClearSearchButton);
            return new SearchResults(Driver);
        }

        public Homepage ChangeLanguageToEnglish()
        {
            //TODO: future improvement - parametrize it to be able to change to any language
            Driver.WaitUntilClickable(Minimap);
            Driver.WaitAndClick(MenuButton);
            Driver.WaitAndClick(LanguageButton);
            Driver.WaitAndClick(EnglishLanguageItem.TagName.Equals("a") ? EnglishLanguageItem : CloseLanguagePanelButton);
            Driver.WaitUntilClickable(SeeTraficButton);
            return this;
        }

        public Homepage ClickSearchField()
        {
            Driver.WaitAndClick(SearchField);
            return this;
        }
    }
}