﻿using NUnit.Framework;
using OpenQA.Selenium;
using WoogaTestChallenge.Extensions;
using WoogaTestChallenge.WebDriver;

namespace WoogaTestChallenge.Tests
{
    [TestFixture]
    public class BaseTest
    {
        protected IWebDriver Driver { get; set; }

        [SetUp]
        public void Setup()
        {
            Driver = new WebDriverFactory().GetWebDriver();
            Driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void TearDown()
        {
            Driver?.TakeScreenshot();
            Driver?.Quit();
        }
    }
}
