﻿namespace WoogaTestChallenge.Config
{
    public class DriverConfig
    {
        public string Browser { get; set; }
        public string Language { get; set; }
        public string BaseUrl { get; set; }
        public int DefaultTimeout { get; set; }
        public string BrowserType { get; set; }
        public bool Headless { get; set; }
    }
}