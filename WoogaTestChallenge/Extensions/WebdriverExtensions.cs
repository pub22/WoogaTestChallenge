﻿using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using WoogaTestChallenge.Config;

namespace WoogaTestChallenge.Extensions
{
    public static class WebdriverExtensions
    {
        public static void GoTo(this IWebDriver driver, string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public static void GoBack(this IWebDriver driver)
        {
            driver.Navigate().Back();
        }

        public static void GoForward(this IWebDriver driver)
        {
            driver.Navigate().Forward();
        }

        public static WebDriverWait Wait(this IWebDriver driver)
        {
            return new WebDriverWait(driver, TimeSpan.FromSeconds(ConfigReader.DriverConfig.DefaultTimeout));
        }

        public static void WaitUntilClickable(this IWebDriver driver, By by)
        {
            driver.Wait().Until(ExpectedConditions.ElementToBeClickable(by));
        }

        public static void WaitUntilClickable(this IWebDriver driver, IWebElement element)
        {
            driver.Wait().Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static IWebElement WaitAndFind(this IWebDriver driver, By by)
        {
            driver.WaitUntilClickable(by);
            return driver.FindElement(by);
        }

        public static void WaitAndClick(this IWebDriver driver, IWebElement element)
        {
            driver.WaitUntilClickable(element);
            element.Click();
        }

        public static void WaitAndSendKeys(this IWebDriver driver, IWebElement element, string keys)
        {
            driver.WaitUntilClickable(element);
            element.Clear();
            element.SendKeys(keys);
        }

        public static void TakeScreenshot(this IWebDriver driver)
        {
            var testName = NUnit.Framework.TestContext.CurrentContext.Test.Name;
            var folder = "Screenshots";

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            var path = Path.Combine(folder, $"screen_{testName}_{DateTime.Now}.png");
            path = path.Replace(" ", "_").Replace(":", "-");

            var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
        }
    }
}